import React, {Component} from "react";
import firebase from "../../Firebase";
import '../../App.css'

class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            titulo: "Login",
            email: "",
            senha: "",
            mensagem: ""
        }

        this.acessar = this.acessar.bind(this);
    }

    async acessar() {

        firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.senha)
        .then( () => {
            window.location.href = "./principal";
        }).catch(()=>{
            this.setState({mensagem: 'Usuário ou senha incorretos'})
        });
    }

    render(){
        return(
            <header className='App-header'>
                <div class='App-header-content'>
                    <h1>{this.state.titulo}</h1>
                    <input type="text" id="email" name="email" placeholder="E-mail" onChange={ (e) => this.setState({email: e.target.value})}/>
                    <input type="password" id="senha" name="senha" placeholder="Senha" onChange={ (e) => this.setState({senha: e.target.value})}/>
                    <button onClick={this.acessar} id="acesso">Acessar</button>
                    <label for='acesso' class='mensagemAcesso'>{this.state.mensagem}</label>
                </div>
            </header>
        )
    }
}

export default Login;