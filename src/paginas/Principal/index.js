import React, {Component} from "react";
import firebase from "../../Firebase";
import '../../App.css'

class Principal extends Component {
    constructor(props){
        super(props);
        this.state = {
            titulo: "Home",
            nome: '',
            sobrenome:'',
            dataNascimento: '',
        }

        this.sair = this.sair.bind(this);
    }

    async sair() {

        await firebase.auth().signOut()
        .then( () => {
            window.location.href = "./login";
        })
        .catch((erro)=>{

        });
    }

    async componentDidMount(){

        firebase.auth().onAuthStateChanged( async (usuario) => {
            if(usuario){
                var uid = usuario.uid;

                await firebase.firestore().collection("usuario").doc(uid).get()
                .then((retorno) => {
                    this.setState({
                        nome: retorno.data().nome,
                        sobrenome: retorno.data().sobrenome,
                        dataNascimento: retorno.data().dataNascimento
                    })
                })
            }
        })

    }
    
    render(){
        return(
            <header className='App-header'>
                <div class='App-header-content'>
                    <p>Seja bem vindo(a), {this.state.nome}!</p>
                    <div class='App-user-title'>Dados do Usuário</div>
                    <div class="App-user-card">
                        {this.state.nome + " " + 
                        this.state.sobrenome + " - Data de Nascimento: " + this.state.dataNascimento} 
                      
                    </div>
                    <button onClick={this.sair}>Sair</button>
                </div>
            </header>
        )
    }
}

export default Principal;