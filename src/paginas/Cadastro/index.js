import React, {Component} from "react";
import { Link } from "react-router-dom";
import firebase from "../../Firebase";

import '../../App.css';

class Cadastro extends Component {
    constructor(props){
        super(props);
        this.state = {
            titulo: "Registre-se"
        }

        this.gravar = this.gravar.bind(this);
        this.logar = this.logar.bind(this);
    }

    async logar() {
        window.location.href = './login';
    }

    async gravar(){

        await firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.senha)
        .then(
            async (retorno) => {

                await firebase.firestore().collection("usuario").doc(retorno.user.uid).set({
                    nome: this.state.nome,
                    sobrenome: this.state.sobrenome,
                    dataNascimento: this.state.dataNascimento,
                })
            }
        )
        .then (() => {
            window.location.href = "./login";
        })
       
    }

    render(){
        return(
            <header className='App-header'>
                <div class='App-header-content'>
                    <h1>{this.state.titulo}</h1>
                    <input type="text" id="nome" name="nome" placeholder="Nome" onChange={ (e) => this.setState({nome: e.target.value})}/>
                    <input type="text" id="sobrenome" name="sobrenome" placeholder="Sobrenome" onChange={ (e) => this.setState({sobrenome: e.target.value})}/>
                    <input type="date" id="dataNascimento" name="dataNascimento" placeholder="Data de Nascimento" onChange={ (e) => this.setState({dataNascimento: e.target.value})}/>
                    <input type="text" id="email" name="email" placeholder="E-mail" onChange={ (e) => this.setState({email: e.target.value})}/>
                    <input type="password" id="senha" name="senha" placeholder="Senha" onChange={ (e) => this.setState({senha: e.target.value})}/>
                    <button onClick={this.gravar}>Registrar</button>
                    <button onClick={this.logar}>Login</button>
                </div>
            </header>
        )
    }
}

export default Cadastro;