import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyCr2s1is3Zy-KFDCiCrHolHCtHbwup84MM",
    authDomain: "react-app-fc477.firebaseapp.com",
    projectId: "react-app-fc477",
    storageBucket: "react-app-fc477.appspot.com",
    messagingSenderId: "305761093689",
    appId: "1:305761093689:web:6832ba589eb010b21565e8"
  };

  if(!firebase.apps.length){
    firebase.initializeApp(firebaseConfig);
  }

  export default firebase;
  