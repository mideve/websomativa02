import React from "react";
import Principal from "./paginas/Principal";
import Cadastro from "./paginas/Cadastro";
import Login from "./paginas/Login";

import { BrowserRouter as Router, Routes, Route} from "react-router-dom";

const Rotas = () => {
    return (
        <Router>
            <Routes>
                <Route path="/" element={<Cadastro/>}></Route>
                <Route path="/login" element={<Login/>}></Route>
                <Route path="/principal" element={<Principal/>}></Route>
            </Routes>
        </Router>
    )
}

export default Rotas
